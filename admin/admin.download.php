<?php

////

	echo '<ul id="navopcjons">
				<li'.($_GET['admin'] == 'add' ? ' class="on"' : null).'>
					<a href="/admin/?db='.$_GET['db'].'&admin=add">Dodaj</a></li>
				<li'.($_GET['admin'] == 'browse' || $_GET['admin'] == false ? ' class="on"' : null).'>
					<a href="/admin/?db='.$_GET['db'].'&admin=browse">Przeglądaj</a></li>'
				.($_GET['admin'] == 'edit' ? '<li'.($_GET['admin'] == 'edit' ? ' class="on"' : null).'>
					<a href="/admin/?db='.$_GET['db'].'&admin=edit&id='.$_GET['id'].'">Edycja</a></li>': null)
			.'</ul>'
			
	.'<div class="box-740">';

////

		if($_POST['option'] == 'add')
			$alert = add_download();
		
		if($_GET['option'] == 'delete')
			$alert = delete_download();
		
		if($_POST['option'] == 'update')
			$alert = update_download();
		
		if($alert)
			echo alert_msg($alert);

////

	if($_GET['admin'] == 'add')
	{
		if(!$alert['ok'])
		{
			echo '<form class="form-uni" onsubmit="onSubmit(1);"  enctype="multipart/form-data" method="post">
					<div class="box-input"> 
						<label for="name">Nazwa pliku:</label>
						<input id="name" class="big" type="text" name="name" value="'.$_POST['name'].'" />
					</div>
					<div class="box-input"> 
						<label for="description">Opis:</label>
						<textarea id="description" class="big"  name="description">'.$_POST['description'].'</textarea>
					</div>
					<div class="box-input"> 
						<label for="file">Plik:</label>
						<input id="file" class="file" type="file" name="file" />
					</div>
					<p class="info">w przypadku braku miniatury, użyta zostanie domyślna dla typu pliku</p>
					<div class="box-input"> 
						<label for="image">Miniaturka:</label>
						<input id="image" class="file" type="file" name="image" />
					</div>
					<div class="box-submit">
						<div class="right">
							<input type="hidden" name="option" value="add" />
							
							<input class="submit" id="submit_1" type="submit" value="Dodaj" />
						</div>
					</div>
				</form>';
		
		}
	}

////
	
	if($_GET['admin'] == 'edit')
	{
		$sql_row = mysql_fetch_array(mysql_query("SELECT * FROM wokar_download WHERE id = '".$_GET['id']."'"));
	
		echo '<form class="form-uni" onsubmit="onSubmit(1);"  enctype="multipart/form-data" method="post">
				<div class="box-input"> 
					<label for="name">Nazwa pliku:</label>
					<input id="name" class="big" type="text" name="name" value="'.$sql_row['name'].'" />
				</div>
				<div class="box-input"> 
					<label for="description">Opis:</label>
					<textarea id="oferta" class="big"  name="description">'.$sql_row['description'].'</textarea>
				</div>
				<div class="box-input"> 
					<label for="file">Plik:</label>
					<a style="float:left; display: block;" href="/src/download/'.$sql_row['filename'].'" 
						target="_blank">/src/download/'.$sql_row['filename'].'</a>
				</div>
				
				<div class="box-input">
					<label for="imageview">Miniaturka:</label>
							<img style="float:left;" src="'.(file_exists('../src/download/thumb_'.$sql_row['id'].'.jpg') ?
											'../src/download/thumb_'.$sql_row['id'].'.jpg?noCache='.time()
										:downloadIconsFiles($sql_row['filename'])).'" alt="" />
				</div>
				<p class="info">w przypadku braku miniatury, użyta zostanie domyślna dla typu pliku</p>
				<div class="box-input"> 
					<label for="image">Zmień miniaturkę:</label>
					<input id="image" class="file" type="file" name="image" />
				</div>
				<div class="box-submit">
					<div class="right">
						<input type="hidden" name="filename" value="'.$sql_row['filename'].'" />
						<input type="hidden" name="row" value="'.$sql_row['id'].'" />
						<input type="hidden" name="option" value="update" />
						
						<input class="submit" id="submit_1" type="submit" value="Aktualizuj" />
					</div>
				</div>
			</form>';
			
	}

////

	if($_GET['admin'] == 'browse' || $_GET['admin'] == false)
	{
		$sql_count = mysql_result(mysql_query("SELECT COUNT(id) FROM wokar_download"),0);
		
		$paging = my_paging( array (
		
				'count' => $sql_count,
				'page' => 6,
				'bar' => 4,
				'url' => 'admin/?db='.$_GET['db'].'&s=',
				's' => $_GET['s'], 
				'title' => 'Do pobrania'
		));
		
		$sql_result = mysql_query("SELECT * FROM wokar_download ORDER BY id DESC LIMIT ".$paging['start'].", ".$paging['on_page']);
		
		if(0 < $sql_count)
		{
			echo '<div class="tabela">
					<div class="tr na">
						<div class="td" style="width: 20%;">Miniaturka</div>
						<div class="td" style="width: 20%;">Nazwa pliku</div>
						<div class="td" style="width: 37%;">Opis</div>
						<div class="td" style="width: 10%;">Opcje</div>
					</div>';

				while ($row = mysql_fetch_array($sql_result))
				{
					echo '<div class="tr">
							<div class="td" style="width: 20%;">
								<img src="'.(file_exists('../src/download/thumb_'.$row['id'].'.jpg') ?
												'../src/download/thumb_'.$row['id'].'.jpg'
											:downloadIconsFiles($row['filename'])).'" alt="" />
							</div>
							<div class="td" style="width: 20%;">'.$row['name'].'</div>
							<div class="td" style="width: 37%;">'.$row['description'].'</div>
							<div class="td" style="width: 10%;">
								<a class="botton-edytuj botton-spacer" title="Edytuj" 
									href="/admin/?db='.$_GET['db'].'&admin=edit&id='.$row['id'].'">Edytuj</a>
								<a class="botton-usun" title="Usuń" 
									href="/admin/?db='.$_GET['db'].'&option=delete&id='.$row['id'].'&filename='.$row['filename'].'&admin=browse&s='.$_GET['s'].'">Usuń</a>
							</div>
						</div>';
				
				}
				
			echo '</div>';
			
			echo $paging['links'];
			
		} else echo alert_msg(array('ok' => 'Brak'));
	
	}

	echo '</div>';
?>
