<?php

////

	require ('../etc/driver.php');
	require ('../etc/function.html.php');
	require ('../etc/function.admin.php');

	//połączenie z bazą
	$connect = my_mysql_connect();

	if($_GET['db'] ? true : $_GET['db'] = 'pulpit');
	
	if($_POST['passwd'])
	{
		if($_POST['passwd'] == $config_admin['passwd'])
			$_SESSION['passwd'] = $_POST['passwd'];

	}

	if ($_GET['db'] == 'logout')
	{
		session_unset();
		session_destroy();
	}
	
	//nagłówek
	$admin_h1 = 'Kancelaria Adwokacka Katarzyna Kowalczyk';
	
	//menu
	$admin_menu = array (
		'ustawienia'				=> array ('ustawienia strony', 'Ustawienia strony'),
		'kancelaria'				=> array ('kancelaria', 'Kancelaria'),
		//'offer'					=> array ('oferta', 'Oferta'),
		'omnie'						=> array ('o mnie', 'O mnie'),
		'zakres'					=> array ('zakres', 'Zakres'),
		/*'linki'						=> array ('linki', 'Linki'),
		'galeria'					=> array ('galeria', 'Galeria'),*/
		'blog-kategorie'			=> array ('blog kategorie', 'Blog kategorie'),
		'blog'						=> array ('blog', 'Blog'),
		'mediacje'					=> array ('mediacje', 'Mediacje'),
		'kontakt'					=> array ('kontakt', 'Kontakt'),

	);
	


	echo '<!DOCTYPE html>
				<html lang="pl">
				<head>
					<meta charset=utf-8 />
					<title>Admin - '.$_GET['db'].'</title>
					
					<link rel="stylesheet" href="/css/admin.css" type="text/css" />
					<link rel="stylesheet" href="/css/lightbox.css" />
					
					<link rel="stylesheet" href="../beta/assets/css/bootstrap.min.css">
					<link rel="stylesheet" href="../beta/assets/css/font-awesome.min.css">
					
					<link href="http://fonts.googleapis.com/css?family=Open+Sans&subset=latin,latin-ext" rel="stylesheet" type="text/css">
					
					<script type="text/javascript" src="/js/main.js?ver=2.09"></script>
					
					<script type="text/javascript" src="/js/prototype.js"></script>
					<script type="text/javascript" src="/js/scriptaculous.js?load=effects,builder"></script>
					<script type="text/javascript" src="/js/lightbox.js"></script>
					<script type="text/javascript" src="/js/tiny_mce/tiny_mce.js"></script>
					<script type="text/javascript" src="/js/jscolor/jscolor.js"></script>
					<script type="text/javascript" src="/js/main.js"></script>

				</head>
				<body>';

	if($config_admin['passwd'] == $_SESSION['passwd'])
	{
		echo '<div id="content">

				<div id="bloc-left-200">

					<div id="menu">
						<ul>
							
							<li class="logo"><a href="/"><img src="../img/logosmalllight.png"></a></li>
							<li><a style="margin: 2px 0 15px 0;color: #FFF;" href="?db=pulpit"> <i class="aboutus-icon fa fa-home"></i>&nbsp; Strona główna</a>
							</li>';
							
							while (list ($key, $row) = each ($admin_menu))
							{
								echo '<li'.($key == $_GET['db'] ? ' class="on"' : null).'>
										<a href="?db='.$key.'"> <i class="aboutus-icon fa fa-pencil-square-o"></i>&nbsp; '.$row[0].' </a></li>'; 
							}
							
					echo '<li><a style="margin: 15px 0 0 0;color: #FFFFFF;background-color: #C12626;border-radius: 31px;margin: 44px;" href="?db=logout"> <i class="aboutus-icon fa fa-sign-out"></i>&nbsp; wyloguj</a></li>
						</ul>
					</div>
				
				</div>
				
				<div id="bloc-right-740">';

						////////////////// include /////////////////////
						
						if (file_exists('./admin.'.$_GET['db'].'.php'))
						{
							include ('./admin.'.$_GET['db'].'.php');
							
						}else {

							echo alert_msg(array('ok' => 'Brak podstrony'));

						}
						
						//////////////////////////////////////
				
				echo '</div>
				<div align="right" style="position:fixed; top:0; right:0;">
					<a href="http://www.intelsoft.pl"><img src="../img/poweredby.png"/></a>
				</div>
				
			</div>';
			
	} else {
	

		echo '
		
				<div id="box-login">
				
					<div align="center">
						<img src="../img/logo.png" style="width: 150px;margin: 9px;">
			 		</div>
					
					<form class="form-login" onsubmit="onSubmit(1);" action="/admin/" name="forma" method="post">
						<div class="box-input"> 
							<input id="passwd" class="big" type="password" name="passwd" placeholder="Wpisz hasło..." value="" />
						</div>
						<div class="box-login-submit">
							<div class="right">
								<input class="submit" id="submit_1" type="submit" value="Zaloguj" />
							</div>
						</div>
					</form>
				</div>';
				
	}


	echo '</body>
				</html>';
				
	
	mysql_close($connect);	

?>
