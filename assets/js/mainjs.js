

/****** FIT BOXES *******/	
	boxes = $(".shadow-hov");
	maxHeight = Math.max.apply(Math, boxes.map(function () {
		return $(this).height()
	}).get());
	boxes.height(maxHeight);
	

/****** LIVE SEARCH *******/
	$("#filter").keyup(function() {
		// Retrieve the input field text and reset the count to zero
		var filter = $(this).val(),
		  count = 0;
		
		// Loop through the comment list
		$("#lsearchlist nav ul li").each(function() {
		
		  // If the list item does not contain the text phrase fade it out
		  if ($(this).text().search(new RegExp(filter, "i")) < 0) {
			$(this).fadeOut();
		
			// Show the list item if the phrase matches and increase the count by 1
		  } else {
			$(this).show();
			count++;
		  }
		});
		// Update the count
		var numberItems = count;
		$("#filter-count").text("Liczba znalezionych postów = " + count);
  	});


/****** tooltip *******/

    $('[data-toggle="tooltip"]').tooltip(); 
	


/****** Modal *******/

	$(document).ready(function() {
			$("#fireme").click(function(){
				$("#EnSureModal").modal();
			});
		});