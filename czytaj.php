<?php

	include ('etc/driver.php');
	require ('etc/function.html.php');

	// Header & menu
	/******************************/
	include ('etc/top.php');
	/******************************/
	
	$getpostid = mysql_fetch_array(mysql_query("
		SELECT id_blog, postid
		FROM getpostid
		WHERE postid= '".$_GET['postid']."'"));
	
	$sql_row = mysql_fetch_array(mysql_query("
		SELECT 	b.id_blog,
				b.title,
				b.description,
				b.full_description,
				b.date_add,
				b.priority,
				bc.id_blog_cat,
				bc.title AS cat,
				DATE_FORMAT(b.date_add, '%d.%m.%Y %T') AS date1, 
				DATE_FORMAT(b.date_add, '%d.%m.%Y') AS date2
		FROM blog b
		INNER JOIN blog_cat bc ON bc.id_blog_cat = b.id_blog_cat
		WHERE b.id_blog = '".$getpostid['id_blog']."'"));

	echo '
    <div class="nk-main">


		<div class="nk-gap-4 mt-9"></div>


        <div class="container">
            <div class="nk-portfolio-single">
				
				
				
                <div class="nk-gap-1 mb-14"></div>
                <h1 class="nk-portfolio-title display-4">'.$sql_row['title'].'</h1>
                <div class="row vertical-gap">
                    <div class="col-lg-8">
						
						<!--<img class="nk-img-fit" src="img/blurbg1.png" style="width:75%; padding: 0 0 5%;">-->
						'
						.(file_exists('src/blog/thumb_'.$sql_row['id_blog'].'.jpg') ?
													 '<img src="src/blog/'.$sql_row['id_blog'].'.jpg" alt="" class="nk-img-fit" style="margin-bottom:35px;">'
													:'<img src="img/blurbgblog.png" alt="" class="nk-img-fit" style="margin-bottom:35px;">
													').
						
						'
		
						<div class="nk-portfolio-info">
							<div class="nk-portfolio-text">
                                '.html_entity_decode($sql_row['full_description']).'
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <table class="nk-portfolio-details">
                            <tr>
                                <td>
                                    <strong>Autor:</strong>
                                </td>
                                <td>Katarzyna Kowalczyk</td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Data:</strong>
                                </td>
                                <td>'.$sql_row['date1'].'</td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Udostępnij:</strong>
                                </td>
                                <td>
								<!--
                                    <a href="#" title="Share page on Facebook" data-share="facebook">Facebook</a>,
                                    <a href="#" title="Share page on Twitter" data-share="twitter">Twitter</a>,
                                    <a href="#" title="Share page on Pinterest" data-share="pinterest">Pinterest</a>
								-->	
									<div class="fb-share-button" data-href="http://adwokatsosnowiec.eu/beta/czytaj.php?postid='.$getpostid['postid'].'" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fadwokatsosnowiec.eu%2Fbeta%2Fczytaj.php%3Fpostid%3D'.$getpostid['postid'].'&amp;src=sdkpreparse">Udostępnij</a></div>
									
									
									
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="nk-gap-4 mt-14"></div>

            </div>
        </div>
		
        <!--<img class="nk-img-fit" src="img/blurbg1.png">-->
		
		
		<!-- START: Comments -->
        <div id="comments"></div>
        <div class="nk-comments">
            <div class="nk-gap-3"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        <h3 class="nk-title">Komentarze:</h3>
                        <div class="nk-gap-1"></div>

                        <!-- START: Comment -->';
  ?>                      
						<div id="disqus_thread"></div>
						<script>
						
						/**
						*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
						*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
						
						/*var disqus_config = function () {
						this.page.url = <?php echo $_SERVER[HTTP_HOST] ?>;  // Replace PAGE_URL with your page's canonical URL variable
						this.page.identifier = <?php echo $_GET['postid'] ?>; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
						};*/
						
						(function() { // DON'T EDIT BELOW THIS LINE
						var d = document, s = d.createElement('script');
						s.src = 'https://adwokatsosnowiec-eu.disqus.com/embed.js';
						s.setAttribute('data-timestamp', +new Date());
						(d.head || d.body).appendChild(s);
						})();
						</script>
						<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">
                        comments powered by Disqus.</a></noscript>
                            
                        <!-- END: Comment -->

                        
                    </div>
                </div>
            </div>
            <div class="nk-gap-3"></div>
        </div>
        <!-- END: Comments -->

<?php		
	echo '	
        <!-- START: Pagination -->
        <!--<div class="nk-pagination nk-pagination-center">
            <div class="container">
                <a class="nk-pagination-prev" href="#">
                    <span class="pe-7s-angle-left"></span> Poprzedni post</a>
                <a class="nk-pagination-center" href="#">
                     <span class="nk-icon-squares"></span> 
                </a>
                <a class="nk-pagination-next" href="#">Kolejny post <span class="pe-7s-angle-right"></span> </a>
            </div>
        </div>-->
        <!-- END: Pagination -->
		';


	// Footer
	/******************************/
	include ('etc/footer.php');
	/******************************/
?>