<?php

# SZABLON HTML

	# uniwersalne box'y
	function html_box_top($typ, $rozmiar, $title, $box, $links)
	{
		return '
			<div class="box-'.$rozmiar.'">
				<div class="header">
					'.$title.'
					'.$links.'
				</div>
				<div class="'.($box == 'media scrol' ? ' ' : 'box ').$box.'">
				';
	}
	
	# uniwersalne zamykanie box'ów
	function html_box_bottom()
	{
		return '
			</div>
				</div>
				';
	}
	
	# alert IE > 5,6
	function none_ie()
	{
		$browser_info = strtolower($_SERVER['HTTP_USER_AGENT']);
		if(strpos($browser_info, "msie 6") || strpos($browser_info, "msie 5") || strpos($browser_info, "msie 4"))
		{
			return  '<style tyle="text/css">
						#BrowserNone {
							margin: 20px auto;
							width :100%;
							text-align: center;
							background: #FFFF80;
						} #BrowserNone h1 {
							font-size: 18px;
							color: #000;
						}
						</style>
						<div id="BrowserNone">
							<h1>Twoja przeglądarka nie jest już obsługiwana. Uaktualnij przeglądarkę do nowszej wersji.</h1>
						</div>';
		}
	}
	
	# aletr
	function alert_msg($msg)
	{
		if($msg['ok'])
			return '<div class="alert alert-tak animated flash">'.$msg['ok'].'</div>';
		else
			return '<div class="alert alert-nie animated shake">'.$msg['error'].'</div>';
	}

?>
