<?php

	function resize_image($szerokosc, $wysokosc, $zrodlowa, $typ, $docelowa)
	{
		list($szerokosc_orig, $wysokosc_orig) = getimagesize($zrodlowa);
		switch($typ) {
			case 'image/gif': $fotka = @imagecreatefromgif($zrodlowa); break;
			case 'image/jpeg': $fotka = @imagecreatefromjpeg($zrodlowa); break;
			case 'image/png': $fotka = @imagecreatefrompng($zrodlowa); break;
		}
		if($szerokosc !== false && $wysokosc !== false)
		{
		
			$ratio_orig = $szerokosc_orig/$wysokosc_orig;
				if ($szerokosc/$wysokosc > $ratio_orig) {
				   $szerokosc = $wysokosc*$ratio_orig;
				} else {
				   $wysokosc = $szerokosc/$ratio_orig;
				}
				
			$nowa_fotka = imagecreatetruecolor($szerokosc, $wysokosc);
			@imagecopyresampled($nowa_fotka, $fotka, 0, 0, 0, 0, $szerokosc, $wysokosc, $szerokosc_orig, $wysokosc_orig);
			
		} else {
			
			$nowa_fotka = imagecreatetruecolor($szerokosc_orig, $wysokosc_orig);
			@imagecopyresampled($nowa_fotka, $fotka, 0, 0, 0, 0, $szerokosc_orig, $wysokosc_orig, $szerokosc_orig, $wysokosc_orig);
	
		}
		
		imagejpeg($nowa_fotka, $docelowa, 80);
		@imagedestroy($nowa_fotka);
		@imagedestroy($fotka);
	}
	

	function resize_thumb($nw, $nh, $source, $stype, $dest)
	{
		 $size = getimagesize($source);
		 $w = $size[0];
		 $h = $size[1];
		 switch($stype) {
			  case 'image/gif': $simg = imagecreatefromgif($source); break;
			  case 'image/jpeg': $simg = imagecreatefromjpeg($source); break;
			  case 'image/png': $simg = imagecreatefrompng($source); break;
		 }
		 $dimg = imagecreatetruecolor($nw, $nh);
		 $wm = $w/$nw;
		 $hm = $h/$nh;
		 $h_height = $nh/2;
		 $w_height = $nw/2;
		 
		if($w> $h) {
			$adjusted_width = $w / $hm;
			$half_width = $adjusted_width / 2;
			$int_width = $half_width - $w_height;
			imagecopyresampled($dimg,$simg,-$int_width,0,0,0,$adjusted_width,$nh,$w,$h);
		} elseif(($w <$h) || ($w == $h)) {
			$adjusted_height = $h / $wm;
			$half_height = $adjusted_height / 2;
			$int_height = $half_height - $h_height;
			imagecopyresampled($dimg,$simg,0,-$int_height,0,0,$nw,$adjusted_height,$w,$h);
		} else {
			imagecopyresampled($dimg,$simg,0,0,0,0,$nw,$nh,$w,$h);
		}
		
		imagejpeg($dimg, $dest,75);
		imagedestroy($simg);
		imagedestroy($dimg);
	}
?>
