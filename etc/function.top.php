<?php

//połączenie z bazą

	function my_mysql_connect()
	{
		global $config_mysql;
		
		$connect = @mysql_connect($config_mysql['host'], $config_mysql['user'], $config_mysql['passwd']) or die('SQL ERROR: Bledne haslo');
			mysql_query($config_mysql['set_names']);
			mysql_SELECT_db($config_mysql['db']) or die ('SQL ERROR: Brak bazy danych');

	return $connect;		
	}
	
	function my_mysql_connect2()
	{
		global $config_mysql2;
		
		$connect2 = @mysql_connect($config_mysql2['host'], $config_mysql2['user'], $config_mysql2['passwd']) or die('SQL ERROR: Bledne haslo');
			mysql_query($config_mysql2['set_names']);
			mysql_SELECT_db($config_mysql2['db']) or die ('SQL ERROR: Brak bazy danych');

	return $connect2;		
	}

//abepieczenie zmiennych

	function fuse($var)
	{
		$var = str_replace('\'', '&#039;', htmlspecialchars($var));
		$var = str_replace('\&quot;', '&quot;', $var);
		$var = str_replace('\&#039;', '&#039;', $var);
		
	return trim($var);
	} 


//parser url

	function parserUrl()
	{
		$request = explode('?',$_SERVER['REQUEST_URI']);
		$var = explode(',', $request[1]);
		
			if($var[0]) $_GET['page'] = $var[0];
			if($var[1]) $_GET['s'] = $var[1];
			
		return $_GET;
	}

//kat url

	function fileUrl($in)
	{
		$utf = Array(
		//UTF
		 "\xc4\x85" => "a", "\xc4\x84" => "A", "\xc4\x87" => "c", "\xc4\x86" => "C",
		 "\xc4\x99" => "e", "\xc4\x98" => "E", "\xc5\x82" => "l", "\xc5\x81" => "L",
		 "\xc3\xb3" => "o", "\xc3\x93" => "O", "\xc5\x9b" => "s", "\xc5\x9a" => "S",
		 "\xc5\xbc" => "z", "\xc5\xbb" => "Z", "\xc5\xba" => "z", "\xc5\xb9" => "Z",
		 "\xc5\x84" => "n", "\xc5\x83" => "N");

		$replace = myStrtolower(strtr($in, $utf));
		$replace = str_replace("#039", "", trim($replace));
		$replace = str_replace("&quot;", "", $replace);
		$replace = str_replace(" ", "-",$replace);
		$replace = str_replace("_", "-",$replace);
		$replace = str_replace("--", "-",$replace);
		$replace = str_replace("--", "-",$replace);

	return  preg_replace("/[^a-zA-Z0-9\-\.]/", "", $replace);
	}

//file size

	function myFileSize($file)
	{
		$FZ = $file;
		$FS = array("B","kB","MB","GB","TB","PB","EB","ZB","YB");
	   
		if($FZ > 0)
			return number_format($FZ/pow(1024, $I=floor(log($FZ, 1024))), ($i >= 1) ? 2 : 0) . ' ' . $FS[$I];
		else 
			return $FZ . 'B';
	}
	
// thumbs download ext

	function downloadIconsFiles($patch)
	{
		switch (strtolower(strrchr($patch, '.')))
		{
			default   : $patchThumb = 'null'; break;
			case '.doc': $patchThumb = 'doc'; break;
			case '.docx': $patchThumb = 'doc'; break;
			case '.xls': $patchThumb = 'xls'; break;
			case '.xlsx': $patchThumb = 'xls'; break;
			case '.pdf': $patchThumb = 'pdf'; break;
			case '.rar': $patchThumb = 'zip'; break;
			case '.zip': $patchThumb = 'zip'; break;
			case '.ppt': $patchThumb = 'ppt'; break;
			case '.pptx': $patchThumb = 'ppt'; break;
		}
		
	
	return '/img/file-'.$patchThumb.'.png';
	}

// catalogue, 2344.jpg to /2/2344.jpg

	function src_kat($id){
	return substr($id, 0, 1).'/';
	}

//create folders, 1 to $much, w katalogu $gdzie
	function tworz_katalogi($where, $much)
	{
		if($much < 9999)
		{
			$i=1;
			while ($i <= $much)
			{
				$cat = $where.'/'.$i++;
				if(!is_dir($cat))
				{
					mkdir($cat, 0777);
				}
			}
		}
	}

//aktualny czas
	function aktualny_czas(){
	return date("Y-m-d H:i:s");
	}

//porcjowanie - linki
	function my_paging($in) 
	{
		
		$unless = $in['count'];
		$on_page = $in['page'];
		$on_bar = $in['bar'];
		$url = $in['url'];
		$s = $in['s'];
		$title = $in['title'];
		$gets = $in['gets'];
		
		$s = ($s>1)?number_format($s, 0, "", ""):1;
		
		$stron = ceil($unless/$on_page); 
		if ($s>$stron AND $unless>0) $s = $stron;
		$paging['start'] = ($s-1)*$on_page;
		$paging['on_page'] = $on_page;
		
		if ($s<1) $s=1;
		if ($s>$stron) $s=$stron;$koniec = $s+$on_bar;
		if ($s<=$on_bar) $koniec = $on_bar*2+1;
		if ($koniec>$stron) $koniec = $stron;$start = $koniec-$on_bar*2;
		if ($start<1) $start=1;
		if ($s>1) $p = '<a class="back" href="/'.$url.''.($s-1).$gets.'" title="'.$title.',poprzednia strona">Wstecz</a>';
		if ($s<$stron) $n = '<a class="next" href="/'.$url.''.($s+1).$gets.'" title="'.$title.', następna strona">Dalej</a>';
		for ($i=$start; $i<=$koniec; $i++)
		{
			if ($i==$s) $l .= ' <span>'.$i.'</span>';
			else $l .= ' <a class="num" href="/'.$url . $i . $gets.'" title="'.$title.', '.$i.'">'.$i.'</a>';
		}
		
		if ($stron>1) $paging['links'] .= '<p class="paging">'.$p . $l . $n .'</p>';
		

	return $paging;
	}
	
	
	function my_paging_blog($in) 
	{
		
		$unless = $in['count'];
		$on_page = $in['page'];
		$on_bar = $in['bar'];
		$url = $in['url'];
		$s = $in['s'];
		$title = $in['title'];
		$gets = $in['gets'];
		
		$s = ($s>1)?number_format($s, 0, "", ""):1;
		
		$stron = ceil($unless/$on_page); 
		if ($s>$stron AND $unless>0) $s = $stron;
		$paging['start'] = ($s-1)*$on_page;
		$paging['on_page'] = $on_page;
		
		if ($s<1) $s=1;
		if ($s>$stron) $s=$stron;$koniec = $s+$on_bar;
		if ($s<=$on_bar) $koniec = $on_bar*2+1;
		if ($koniec>$stron) $koniec = $stron;$start = $koniec-$on_bar*2;
		if ($start<1) $start=1;
		if ($s>1) $p = '<a class="back" href="/'.$url.''.($s-1).$gets.'" title="'.$title.',poprzednia strona">Wstecz</a>';
		if ($s<$stron) $n = '<a class="next" href="/'.$url.''.($s+1).$gets.'" title="'.$title.', następna strona">Dalej</a>';
		for ($i=$start; $i<=$koniec; $i++)
		{
			if ($i==$s) $l .= ' <span>'.$i.'</span>';
			else $l .= ' <a class="num" href="/'.$url . $i . $gets.'" title="'.$title.', '.$i.'">'.$i.'</a>';
		}
		
		if ($stron>1) $paging['links'] .= '<p class="paging">'.$p . $l . $n .'</p>';
		

	return $paging;
	}
								
//małe literki

	function myStrtolower($tekst){
	return mb_strtolower($tekst, 'UTF-8');
	}

//duże literki

	function myStrtoupper($tekst){
	return mb_strtoupper($tekst, 'UTF-8');
	}

//entery z bazy

	function enterki($tekst){
	return str_replace("\r\n", " <br />", $tekst);
	}

//bez łamania tekstu

	function bez_enterki($tekst){
	return str_replace("\r\n", " ", $tekst);
	}

//skrócony tekst

	function limit_tekst($tekst,$ile)
	{
		$tekst = str_replace("\r\n", " ", $tekst);

		if (strlen($tekst) > $ile)
		{
			return substr($tekst, 0, strrpos(substr($tekst, 0, $ile), " ")).'...';
		} else {
		
			return $tekst;
		}
	} 

?>
