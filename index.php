<?php
	
	// Strona tymczasowa
	if($_SERVER["REMOTE_ADDR"]<>'89.64.13.76'){
		//header('Location: info.html');
	}
	
	include ('etc/driver.php');
	require ('etc/function.html.php');

	// Header & menu
	/******************************/
	include ('etc/top.php');
	/******************************/
	
	echo '
    <div class="nk-main">
	
	
	

        <!-- START: Header Title -->
        <div class="nk-header-title nk-header-title-full" id="home">
            <div class="bg-image">
                <div style="background-image: url(\'assets/images/bg0.jpg\');"></div>
                <div class="bg-image-overlay" style="background-color: rgba(255, 255, 255, 0);"></div>
            </div>
            <div class="nk-header-table">
                <div class="nk-header-table-cell">
                    <div class="container">

                        <!--<h2 class="nk-subtitle text-white">New Branding Agency</h2>-->


                        <h1 class="nk-title display-3 text-white">
                            <img class="headlogo" src="assets/images/headlogo.png"/>
                            <!--<em class="fw-400">you publish on the web</em>-->
                        </h1>


                        <div class="nk-gap"></div>
                        <div class="nk-header-text text-white">
                            <div class="nk-gap-4"></div>
                        </div>


                    </div>
                </div>
            </div>

            <div>
                <a class="nk-header-title-scroll-down" href="#nk-header-title-scroll-down">
                    <span class="pe-7s-angle-down"></span>
                </a>
            </div>

        </div>

        <div id="nk-header-title-scroll-down"></div>

        <!-- END: Header Title -->';



// Kancelaria //////////////////////////////////////////////
		
		$sql_row = mysql_fetch_array(mysql_query("
		SELECT s.id, s.id_menu, s.tresc, m.nazwa, m.link
		FROM strona s
		INNER JOIN menu m ON m.id = s.id_menu
		WHERE s.id = '3'
		"));
			
		echo '
        <!-- START: Kancelaria -->
        <div class="bg-white" id="kancelaria">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2 text-xs-center">
                        <div class="nk-gap-4 mt-9"></div>

                        <h2 class="display-4">'.$sql_row['nazwa'].'</h2>
                        <div class="nk-gap mnt-5"></div>
						'.html_entity_decode($sql_row['tresc']).'

                        <!--<img src="assets/images/about-me-signature.png" alt="" class="nk-img-fit">-->

                        <div class="nk-gap-4 mt-25"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: Kancelaria -->';
        
        
// O mnie //////////////////////////////////////////////
		
		$sql_row = mysql_fetch_array(mysql_query("
		SELECT s.id, s.id_menu, s.tresc, m.nazwa, m.link
		FROM strona s
		INNER JOIN menu m ON m.id = s.id_menu
		WHERE s.id = '2'
		"));
        
		echo '
        <!-- START: O mnie -->
  
        <div class="bg-gray-1" id="omnie">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2 text-xs-center">
        				<div class="nk-gap-4 mt-9"></div>

                        <h2 class="display-4">'.$sql_row['nazwa'].'</h2>
                        <div class="nk-gap mnt-5"></div>
                        
                        <div class="nk-blog-isotope nk-isotope nk-isotope-gap nk-isotope-1-cols">
                
                
       
                            <div class="nk-isotope-item" data-filter="Nature">
                                <div class="nk-blog-post">
                                    
                                    <!--<h2 class="nk-post-title h4"><a href="blog-single.html">Something I need to tell you</a></h2>
                
                                    <div class="nk-post-date">
                                        September 18, 2016
                                    </div>-->
                
                                    <div class="nk-post-text">
                                        '.html_entity_decode($sql_row['tresc']).'
                                    </div>
                                </div>
                            </div>

   
   
						</div>
                        
                        <div class="nk-gap-4 mt-9"></div>
                        <div class="nk-gap mnt-5"></div>
                        
                    </div>
                </div>
			</div>
        </div>
        <!-- END: O mnie -->
        ';
		
// Zakres //////////////////////////////////////////////        
        
		
								
		echo '
        <!-- START: Zakres -->
        <div class="bg-white" id="zakres">
            <div class="container">
                <!--<div class="row">
                    <div class="col-lg-8 offset-lg-2 text-xs-center">
                        <div class="nk-gap-4 mt-9"></div>

                       <h2 class="display-4">'.$row['nazwa'].'</h2>
                      <div class="nk-gap mnt-5"></div>

                        
                      <div class="nk-gap-4 mt-25"></div>
                    </div>
                
                </div> -->
                <!-- END: Row-->
                
                <div class="row zakres">
                	<div class="col-lg-12 text-xs-center">
                    	<div class="nk-gap-4 mt-9"></div>
                		<h2 class="display-4">Zakres</h2>
                    	<div class="nk-gap mnt-5"></div>';
                    
					$sql_result = mysql_query(" SELECT m.nazwa, sd.*
									FROM strona s
									INNER JOIN strona_dane sd ON sd.id_strona=s.id
									INNER JOIN menu m ON m.id = s.id_menu
									WHERE sd.id_strona=4");
		
					while ($row = mysql_fetch_array($sql_result))
					{
						$zakres .= '
							<div class="col-md-4 col-sm-6 text-center shadow-hov">
									<div class="aboutus-item">
										<i class="aboutus-icon fa fa-gavel" style="color:#'.$row['hex'].';border: 3px solid #'.$row['hex'].';"></i>
										<h4 class="aboutus-title">'.$row['naglowek'].'</h4>
										'.html_entity_decode($row['tresc']).'
									</div>
								</div>
								
								';
					}
		
					echo $zakres;  
					
                    echo '
                    <div class="nk-gap-4 mt-25"></div>
                </div>
                </div>
                <!-- END: Row-->
                
            </div>
            <!-- END: container -->
            <div class="nk-gap-4 mt-25"></div>
        </div>
        <!-- END: Zakres -->
        <div style="clear:both;"></div>';

 		
//Blog //////////////////////////////////////////////	
 	
	echo '
		<!-- START: Blog -->
        <div class="nk-box bg-gray-1" id="blog">
            <div class="nk-gap-4 mt-5"></div>

            <h2 class="text-xs-center display-4">Nowe posty na blogu</h2>

            <div class="nk-gap mnt-6"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        <div class="text-xs-center">
						<a href="blog.php" class="btn btn-secondary btn-lg" style="background-color: #fafafa; border: 1px solid #bdbdbd; color: #9a9a9a;">
						Zobacz więcej postów</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="nk-gap-2 mt-12"></div>
            <div class="container">
                <!-- START: Carousel -->
                <div class="nk-carousel-2 nk-carousel-x2 nk-carousel-no-margin nk-carousel-all-visible nk-blog-isotope" data-dots="true">
                    <div class="nk-carousel-inner">';
					
	$sql_count = mysql_result(mysql_query("SELECT COUNT(id_blog) FROM blog"),0);					
	$sql_result = mysql_query("
				SELECT  g.id_blog_cat, g.title as cat, g.catid, b.id_blog, b.title,
						CASE WHEN CHAR_LENGTH(b.title)>48 THEN CONCAT(SUBSTRING(b.title,1,48),'...') ELSE b.title END AS title_short,
						b.description, b.full_description, 
						b.date_add, DATE_FORMAT(b.date_add, '%d.%m.%Y %T') date1, DATE_FORMAT(b.date_add, '%d.%m.%Y') date2,
						b.priority, gp.postid, CONCAT(SUBSTRING(b.full_description,1,120), '...') AS sdescription
				
				FROM getcatid g
					INNER JOIN blog b ON b.id_blog_cat = g.id_blog_cat
					INNER JOIN getpostid gp ON gp.id_blog = b.id_blog
				ORDER BY b.priority, b.id_blog DESC LIMIT 4");
					
	if(0 < $sql_count)
				{
						
					while ($row = mysql_fetch_array($sql_result))
					{
						echo '
							
						<div>
                            <div>
                                <div class="pl-15 pr-15">
                                    <div class="nk-blog-post">
                                        <div class="nk-post-thumb">
                                            <a href="czytaj.php?postid='.$row['postid'].'">
											'
											.(file_exists('src/blog/thumb_'.$row['id_blog'].'.jpg') ?
											 '<img src="src/blog/'.$row['id_blog'].'.jpg" alt="" class="nk-img-stretch">'
											:'<img src="img/blurbgblog.png" alt="" class="nk-img-stretch">
											').
											'
                                            </a>
                                            <div class="nk-post-category"><a href="#">'.$row['cat'].'</a></div>
                                        </div>
                                        <h2 class="nk-post-title h4"><a href="czytaj.php?postid='.$row['postid'].'">'.$row['title_short'].'</a></h2>

                                        <div class="nk-post-date">
                                            '.$row['date2'].'
                                        </div>

                                        <div class="nk-post-text" id="latestblogdesc">
                                            '.html_entity_decode($row['sdescription']).'
                                        </div>
                                    </div>
                                </div>
                                <div class="nk-gap-1"></div>
                            </div>
                        </div>
						';
					}			
					
					echo '
					
					</div>
                </div>
                <!-- END: Carousel -->
				
            </div>
            <div class="nk-gap-5 mt-20"></div>

        </div>
        <!-- END: Blog -->';
							
		} else echo alert_msg(array('ok' => 'Brak postów'));
					
// Mediacje //////////////////////////////////////////////
		
		$sql_row = mysql_fetch_array(mysql_query("
		SELECT s.id, s.id_menu, s.tresc, m.nazwa, m.link
		FROM strona s
		INNER JOIN menu m ON m.id = s.id_menu
		WHERE s.id = '7'
		"));
			
		echo '
        <!-- START: Mediator -->
        <div class="bg-white" id="mediacje">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2 text-xs-center">
                        <div class="nk-gap-4 mt-9"></div>

                        <h2 class="display-4">'.$sql_row['nazwa'].'</h2>
                        <div class="nk-gap mnt-5"></div>
						'.html_entity_decode($sql_row['tresc']).'

                        <!--<img src="assets/images/about-me-signature.png" alt="" class="nk-img-fit">-->

                        <div class="nk-gap-4 mt-25"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: Mediator -->';
		
 //kontakt //////////////////////////////////////////////
  		
		$kontakt = mysql_fetch_array(mysql_query("
				SELECT *
				FROM kontakt
				WHERE id = '1'
			"));
			
		echo '
        <!-- START: Contact Info -->
        <div class="container" id="kontakt">
            <div class="nk-gap-5"></div>
            <div class="row vertical-gap">
                <div class="col-lg-5">
                    <!-- START: Info -->
                    <h2 class="display-4">Dane kontaktowe</h2>
                    <div class="nk-gap mnt-3"></div>
					
					'.html_entity_decode($kontakt['informacja']).'

                    <ul class="nk-contact-info">
                        <li>
                            <strong>Adres:</strong> 
						</li>
						<li>
							'.$kontakt['ad1'].'<br/> 
							'.$kontakt['ad2'].'<br/>
							'.$kontakt['ad3'].'
						</li>
                        <li>
                            <strong>Telefon:</strong></li>
                        <li>
							'.$kontakt['tel1'].'
						<li>
						</li>
                            <strong>E-mail:</strong>
						</li>
						<li>
							'.$kontakt['email'].'
						</li>
                    </ul>
                    <!-- END: Info -->
                </div>
                <div class="col-lg-7">
                   
				    <!-- START: Form -->
					
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3605.8986743919654!2d19.1247262710673!3d50.27782548396444!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4716daa35ef55de5%3A0x7d0b958936e0c4bb!2sWarszawska+10%2C+Sosnowiec!5e0!3m2!1spl!2spl!4v1512397018670" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					
                    <!--<form action="#" class="nk-form nk-form-ajax">
                        <div class="row vertical-gap">
                            <div class="col-md-6">
                                <input type="text" class="form-control required" name="name" placeholder="Imię i nazwisko">
                            </div>
                            <div class="col-md-6">
                                <input type="email" class="form-control required" name="email" placeholder="Adres e-mail">
                            </div>
                        </div>

                        <div class="nk-gap-1"></div>
                        <input type="text" class="form-control required" name="title" placeholder="Temat / czego dotyczy sprawa">

                        <div class="nk-gap-1"></div>
                        <textarea class="form-control required" name="message" rows="8" placeholder="Treść wiadomości" aria-required="true"></textarea>
                        <div class="nk-gap-1"></div>
                        <div class="nk-form-response-success"></div>
                        <div class="nk-form-response-error"></div>
                        <button class="nk-btn">Wyślij wiadomość</button>
                    </form>-->
                    <!-- END: Form -->
					
                </div>
            </div>
            <div class="nk-gap-5"></div>
        </div>
        <!-- END: Contact Info -->
		';

	// Footer
	/******************************/
	include ('etc/footer.php');
	/******************************/
?>
